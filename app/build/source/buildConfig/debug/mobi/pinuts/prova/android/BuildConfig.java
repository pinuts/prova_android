/**
 * Automatically generated file. DO NOT MODIFY
 */
package mobi.pinuts.prova.android;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String PACKAGE_NAME = "mobi.pinuts.prova.android";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 1;
}
