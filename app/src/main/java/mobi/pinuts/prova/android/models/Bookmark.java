package mobi.pinuts.prova.android.models;

import android.graphics.drawable.Drawable;

public class Bookmark {

	private String mTitle;
	private String mUrl;
	private Drawable mIcon;
	
	public Bookmark(String title, String url, Drawable icon) {
		mTitle = title;
		mUrl   = url;
		mIcon = icon;
	}
	
	public String getTitle() {
		return mTitle;
	}
	
	public String getUrl() {
		return mUrl;
	}
	
	public Drawable getIcon() {
		return mIcon;
	}
}
